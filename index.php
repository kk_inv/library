<?php
require('DB.class.php');

$db = new Db('1017.v.tld.pl', 'baza1017_biblioteka', 'admin1017_biblioteka', 'tugh76rtyA');


// ksiazki najczesciej pozyczane (w tym wypadku we wrzesniu)
$books = $db->query("
   SELECT b.title, COUNT(o.id) 
   FROM books as b
   LEFT JOIN orders as o ON (b.id = o.book_id AND o.order_date BETWEEN '2017-09-01 00:00:00' AND '2017-09-30 23:59:59')
   GROUP BY b.id 
   ORDER BY COUNT(o.id) DESC 
   LIMIT 10");

// var_dump($books);

// userzy z najwieksza iloscia wypozyczen bez przetrzyman
$users = $db->query("
   SELECT u.forename, u.surname, COUNT(o.id), COUNT(f.ID) 
   FROM users as u
   LEFT JOIN orders as o ON ( u.id = o.user_id )
   LEFT JOIN fines as f ON ( f.order_id = o.id )
   GROUP BY u.id 
   HAVING COUNT(f.id) = 0 
   ORDER BY COUNT(o.id) DESC 
   LIMIT 10 ");

//var_dump ( $users );

// 3 dni najczestsze w godzinach 10-16 (w tym wypadku we wrzesniu)
$days = $db->query("
   SELECT COUNT(o.id), DAY(o.order_date)
   FROM orders as o
   WHERE o.order_date BETWEEN '2017-09-01 00:00:00' AND '2017-09-30 23:59:59' 
   AND HOUR(o.order_date) >= 10 AND HOUR(o.order_date) <= 16 
   GROUP BY DAY(o.order_date)
   ORDER BY COUNT(o.id) DESC 
   LIMIT 3 ");

//var_dump ( $days );
